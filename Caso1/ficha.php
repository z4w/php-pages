<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="shortcut icon" href="../icon/icon.svg" type="image/x-icon">
    <link rel="stylesheet" href="css/registro.css">
    <title>ficha</title>
</head>
<body>
    <div class="container">
        <form id="ejemplo" action="procesa_formulario.php" target="_blank" method="post">
            <section class="contact">
                <div class="contact-header form-header" >
                    <h2>DATOS DE CONTACTO</h2>
                    <p>FICHA DE PEDIDO</p>
                </div>
                <div class="contact-form form-boxes">
                    <div class="box-field">
                        <label for="nombre">Nombre <span class="req">*</span></label>
                        <input type="text" name="nombre" placeholder="Nombre">
                    </div>
                    <div class="box-field">
                        <label for="apellido">Apellidos <span class="req">*</span></label>
                        <input type="text" name="apellido" placeholder="Apellidos">
                    </div>
                    <div class="box-field">
                        <label for="contact-celular">Celular <span class="req">*</span></label>
                        <input type="text" name="contact-celular" placeholder="Celular">
                    </div>
                    <div class="box-field">
                        <label for="correo">Correo <span class="req">*</span></label>
                        <input type="text" name="correo" placeholder="Correo electronico">
                    </div>
                </div>
            </section>
            <section class="delivery">
                <div class="delivery-header form-header">
                    <h2>DATOS DE ENTREGA</h2>
                    <p>Por el momento solo ofrecemos recojo en tienda</p>
                </div>
                <div class="delivery-form form-boxes">
                    <div class="box-field">
                        <label for="delivery-fecha">Fecha <span class="req">*</span></label>
                        <input type="date" name="delivery-fecha">
                    </div>
                    <div class="box-field">
                        <label for="delivery-time">Hora de recojo.</label>
                        <input type="time" name="delivery-time">
                    </div>
                    <div class="box-field">
                        <label for="delivery-transporte">Metodo de Transporte <span class="req">*</span></label>
                        <input type="text" name="delivery-transporte">
                    </div>
                    <div class="box-field">
                        <label for="delivery-persona">Persona que lo recogera: <span class="req">*</span></label>
                        <input type="text" name="delivery-persona">
                    </div>
                </div>
            </section>
            <div class="form-submit form-boxes">
                <input type="submit" name="enviar" value="Enviar" />
            </div>
        </form>
    </div>
</body>
</html>