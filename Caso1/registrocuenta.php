<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="css/registro2.css">
    <link rel="shortcut icon" href="../icon/icon.svg" type="image/x-icon">
    <title>Registro</title>
</head>
<body>
    <div class="container">
        <div class="registro">
            <div class="registro-header">
                <h3>Registrar una nueva cuenta</h3>
                <p>Si ya esta registrado ingrese a su cuenta</p>
            </div>
            <form id="ejemplo" action="procesa_formulario.php" target="_blank" method="post">
                <div class="item-field">
                    <label for="nombre">Nombre</label>
                    <input type="text" name="nombre">
                </div>
                <div class="item-field">
                    <label for="apellido">Apellidos</label>
                    <input type="text" name="apellido">
                </div>
                <div class="item-field">
                    <label for="correo">Correo electronico</label>
                    <input type="text" name="correo">
                </div>
                <div class="field-sub">
                    <div class="item-field">
                        <label for="password">Contraseña</label>
                        <input type="text" name="password">
                    </div>
                    <div class="item-field">
                        <label for="repassword">Repetir contraseña</label>
                        <input type="text" name="repassword">
                    </div>
                </div>
                <div class="field-sub">
                    <div class="item-field">
                        <label for="security">Codigo de Seguridad</label>
                        <input type="text" name="security">
                    </div>
                    <div class="item-field">
                        <label for="code">Introduce el codigo</label>
                        <input type="text" name="code">
                    </div>
                </div>
                <div class="item-field">
                    <label for="licencia">No. Licencia. (En Caso de poseerla)</label>
                    <input type="text" name="licencia">
                </div>
                <div class="item-field item-submit">
                    <input type="submit" name="enviar" value="Enviar">
                </div>
            </form>
        </div>
    </div>
</body>
</html>