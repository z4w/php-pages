<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="shortcut icon" href="../icon/icon.svg" type="image/x-icon">
    <link rel="stylesheet" href="css/proceso.css">
    <title>Confirmation</title>
</head>
<body>
    <div class="container">
        <div class="sub-container">
            <div class="content">
                <div class="icon">
                    <img src="img/logo.jpg" alt="Paypal_logo">
                </div>
                <div class="message">
                    <div class="title">
                        <h3>Registro realizado con éxito</h3>
                    </div>
                    <div class="user-data">
                        <div class="user-field">
                            <p class="field-label">Nombre:</p>
                            <?php echo "<p>".$_POST['nombre']."</p>"?>
                        </div>
                        <div class="user-field">
                            <p class="field-label">Correo:</p>
                            <?php echo "<p>".$_POST['email']."</p>"?>
                        </div>
                        <div class="user-field">
                            <p class="field-label">Contraseña:</p>
                            <?php echo "<p>".$_POST['pwd']."</p>"?>
                        </div>
                        <div class="user-field">
                            <p class="field-label">Patente:</p>
                            <?php echo "<p>".$_POST['patente']."</p>"?>
                        </div>
                        <div class="user-field">
                            <p class="field-label">Modelo:</p>
                            <?php echo "<p>".$_POST['coche']."</p>"?>
                        </div>
                    </div>
                    <div class="text">
                        Gracias por su preferencia.
                    </div>
                </div>
            </div>
            <div class="acctions">
                <button class="btn-download">Descargar</button>
                <button class="return">Volver</button>
            </div>
        </div>
    </div>
</body>
</html>