<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="shortcut icon" href="../icon/icon.svg" type="image/x-icon">
    <link rel="stylesheet" href="css/style.css">
    <title>Mis Datos</title>
</head>
<body>
    <div class="container">
        <h1>Mis Datos</h1>
        <form action="mostrardatos.php" method="post" enctype="multipart/form-data">
        <div class="form-field">
            <label for="nombre">Nombre</label>
            <input type="text" name="nombre" placeholder="Su nombre de usuario" minlength="5" maxlength="40 require pattern="[A-Za-Z0-9]+" >
        </div>
        <div class="form-field">
            <label for="email">Email</label>
            <input type="text" name="email" pattern="[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,}$">
        </div>
        <div class="form-field">
            <label for="pwd">contraseña</label>
            <input type="password" id="pwd" name="pwd" pattern="(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{8,}" title="contraseña invalida">
        </div>
        <div class="form-field">
            <label for="patente">Patente</label>
            <input type="text" name="patente" id="patente" required pattern="[A-Za-z]{3}[0-9]{3}">
        </div>
        <div class="form-field">
            <label for="coche">Modelo</label>
            <input type="text" name="coche" placeholder="Su modelo de auto" required pattern="A|a(1|2|3|4|15)" title="Modelos posibles: A1, A3, A4, A15">
        </div>
        <div class="form-field">
            <label for="imagencoche">Foto</label>
            <input name="imagencoche" type="file" class="imagephp">
        </div>
        <div class="form-field sendphp">
            <input class="envio" type="button" name="Boton" Value="Enviar" onClick="submit()">
        </div>    
    </form>
    </div>
</body>
</html>



