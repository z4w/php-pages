<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="shortcut icon" href="../icon/icon.svg" type="image/x-icon">
    <link rel="stylesheet" href="css/style.css">
    <title>Anuncios</title>
</head>
<body>
    <div class="container">
        <div class="header">
            <h2>Anuncio</h2>
        </div>
        <div class="options">
            <form method="post" action="anuncios.php">
                <div class="option-field">
                    <input type="checkbox" name="anuncio[]" id="como1" value="Web">
                    <label for="como1">Una web</label>
                </div>
                <div class="option-field">
                    <input type="checkbox" name="anuncio[]" id="como2" value="Google">
                    <label for="como2">Google</label>
                </div>
                <div class="option-field">
                    <input type="checkbox" name="anuncio[]" id="como3" value="Anuncio en prensa">
                    <label for="como3">Anuncio en prensa</label>
                </div>
                <div class="option-field">
                    <input type="checkbox" name="anuncio[]" id="como4" value="Anuncio en tv">
                    <label for="como4">Anuncio en tv</label>
                </div>
                <div class="option-field">
                    <button class="envio" type="submit" name="submit">Enviar</button>
                </div>
            </form>
        </div>
        <div class="output">
            <?php
                if (isset($_POST['submit'])) {
                    // Obtener los valores del formulario
                    $opciones = $_POST["anuncio"];
                
                    // Verificar si se seleccionaron opciones
                    if (!empty($opciones)) {
                    echo "<h2>Valores:</h2>";
                    echo "<ul>";
                    // Mostrar los valores seleccionados
                    foreach ($opciones as $opcion) {
                        echo "<li>" . htmlspecialchars($opcion) . "</li>";
                    }
                    echo "</ul>";
                    } else {
                    echo "<p>No se seleccionaron opciones.</p>";
                    }
                }      
            ?>
        </div>
    </div>
</body>
</html>